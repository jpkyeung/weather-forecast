# 5-Day Weather Forecast
Simple weather forecast app build using react, redux, thunk, material-ui.

## Assumptions
* Time of data will be dependend on the timezone of the city being searched
* Conditions, Temperaure, Wind are averages of hourly data provided

## Install
`yarn install`

## Run
`yarn start`
Runs the app in the development mode.  
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Test
`yarn test`
Launches the test runner in the interactive watch mode.

## Build
`yarn build`
Builds the app for production to the `build` folder.

## Running the build app
Install serve  
`yarn install -g serve`

Run serve  
`serve -s build`  
Open [http://localhost:5000](http://localhost:5000) to view it in the browser.
