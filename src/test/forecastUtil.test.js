import mockData from './mockData';
import { parseForecastData, toCelsius, toFahrenheit, toKMH, toMPH } from '../utils/forecastUtils';


describe('forecastUtils functions test', () => {

  const forecastData = parseForecastData(mockData);
  const mockC = toCelsius(1)
  const mockF = toFahrenheit(2);
  const mockKMH = toKMH(3);
  const mockMPH = toMPH(4);

  it('parseForecastData should be correct', () => {
    expect(forecastData[0].displayDay).toBe('Today');
    expect(forecastData[0].displayDate).toBe('14/2');
    expect(forecastData[0].info.temp.minTemp).toBe(293.03);
    expect(forecastData[0].info.temp.maxTemp).toBe(296.62);
    expect(forecastData[0].info.condition.main).toBe('Rain');
    expect(forecastData[0].info.condition.description).toBe('light rain');
    expect(forecastData[0].info.condition.icon).toBe('10n');
    expect(forecastData[0].info.condition.count).toBe(4);
    expect(forecastData[0].info.wind.speed).toBe(3.2);
    expect(forecastData[0].info.wind.direction).toBe('ES');
    expect(forecastData[0].details.length).toBe(7);
    expect(forecastData[0].details[0].displayTime).toBe('5:00 AM');
  })

  it('forecast calculations should be correct', () => {
    expect(mockC).toBe(-272.15);
    expect(mockF).toBe(-456.07);
    expect(mockKMH).toBe(10.8);
    expect(mockMPH).toBe(8.948);
  })
})