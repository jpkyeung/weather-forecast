import React from 'react';
import {render} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import ForecastListItem from '../components/ForecastListItem';

describe('ForecastItem component test', () => {
  it('prop content should exist', ()=> {
    const mockData =  {
      title: 'Hello World',
      temp: {
        avgTempC: 11,
        avgTempF: 22,
      },
      condition: {
        main: '',
        icon: '',
      },
    }

    const {getByText} = render(
      <ForecastListItem
        title={mockData.title}
        temp={mockData.temp}
        condition={mockData.condition}
      />
    );

    expect(getByText('Hello World')).toBeInTheDocument();
    expect(getByText('11°C / 22°F')).toBeInTheDocument();
  })
})