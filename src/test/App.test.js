import React from 'react';
import ReactDOM from 'react-dom';
import App from '../App';
import store from '../store';
import { Provider } from 'react-redux';
import {render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'


describe('App component test', () => {

  it('App should render', () => {
    const div = document.createElement('div');
    ReactDOM.render(
      <Provider store={store}>
        <App />
      </Provider>,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
  });

  it('App should have correct title ', () => {
    const {getByText} = render(
      <Provider store={store}><App/></Provider>,
    )
    expect(getByText('Weather Forecast')).toBeInTheDocument()
 });
})