import React from 'react';
import {render} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import ForecastItem from '../components/ForecastItem';

describe('ForecastItem component test', () => {
  it('prop content should exist', ()=> {
    const mockData =  {
      displayDay: 'Hello Day',
      displayDate: '14/2',
      info: {
        temp: {
          avgTempC: 11,
          avgTempF: 22,
          minTempC: 11,
          minTempF: 22,
          maxTempC: 33,
          maxTempF: 44,
        },
        wind: {
          windSpeedKMH: 55,
          windSpeedMPH: 66,
          direction: 'N'
        },
        condition: {
          main: 'Solar',
          icon: '22',
        },
      }
    }

    const {getByText} = render(
      <ForecastItem 
        displayDay={mockData.displayDay}
        displayDate={mockData.displayDate}
        temp={mockData.info.temp}
        wind={mockData.info.wind}
        condition={mockData.info.condition}
      />
    );

    expect(getByText('Hello Day')).toBeInTheDocument();
    expect(getByText('14/2')).toBeInTheDocument();
    expect(getByText('11°C / 22°F')).toBeInTheDocument();
    expect(getByText('33°C / 44°F')).toBeInTheDocument();
    expect(getByText('55kmh / 66mph')).toBeInTheDocument();
    expect(getByText('Solar')).toBeInTheDocument();
  })
})