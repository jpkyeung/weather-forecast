import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchForecast } from './actions/forecastAction';

import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import ForecastItem from './components/ForecastItem';
import ForecastListItem from './components/ForecastListItem';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';
import List from '@material-ui/core/List';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';

const App = () => {
  const dispatch = useDispatch();
  const forecastList = useSelector(state => state.forecastData);
  const [forecast, setForecast] = useState();
  const city = useSelector(state => state.city);
  const error = useSelector(state => state.isError);
  const loading = useSelector(state => state.isFetching);
  const [input, setInput] = useState('');

  const handleChange = e => {
    setInput(e.target.value);
  }

  const handleSubmit = e => {
    e.preventDefault();
    const location = e.target[0].value;
    setForecast();
    dispatch(fetchForecast(location));
  };

  const handleForecastClick = (day) => {
    setForecast(day);
  };

  const handleBack = () => {
    setForecast();
  }

  const useStyles = makeStyles(theme => ({
    container: {
      paddingTop: '20px',
      paddingBottom: '20px',
    },
    grid: {
      paddingTop: '10px',
      paddingBottom: '20px',
      position: 'relative',
    },
    card: {
      marginTop: '20px',
      width: '100%',
    },
    cardContent: {
      paddingLeft: 0,
      paddingRight: 0,
    },
    input: {
      textAlign: 'center',
      '& input': {
        textAlign: 'center'
      }
    },
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    },
    backButton: {
      position: 'absolute',
      left: 0,
      top: 0,
    },
    error: {
      paddingTop: 5,
      color: 'red',
    },
  }));

  const classes = useStyles();

  return (
    <Container maxWidth='sm' align='center' className={classes.container}>
      <Typography variant='h5' component='h1' gutterBottom>
        Weather Forecast
      </Typography>
      <Card className={classes.card}>
        <CardContent>
          <Grid>
            <form onSubmit={e => handleSubmit(e)} noValidate autoComplete='off'>
              <Input onChange={e => handleChange(e)} value={input} placeholder='Enter location' className={classes.input} error={error} />
            </form>
          </Grid>

          {error &&
            <Typography variant='body1' component='p' gutterBottom className={classes.error}>
              City not found
            </Typography>
          }
        </CardContent>
      </Card>
      {forecastList &&
        <Card className={classes.card}>
          <CardContent className={classes.cardContent}>
            <Grid className={classes.grid}>
              <Typography variant='h4' component='h2' align='center'>
                {city.name}
              </Typography>
              {forecast &&
                <div>
                  <IconButton aria-label='back' className={classes.backButton} onClick={handleBack}>
                    <NavigateBeforeIcon fontSize='large' />
                  </IconButton>
                  <ForecastItem
                    displayDay={forecast.displayDay}
                    displayDate={forecast.displayDate}
                    temp={forecast.info.temp}
                    wind={forecast.info.wind}
                    condition={forecast.info.condition}
                    hourlyData={forecast.details}
                  />
                </div>
              }
              {!forecast &&
                <List component='ul'>
                  {forecastList && forecastList.map((day, index) => (
                    <ForecastListItem
                      key={index}
                      onClick={() => handleForecastClick(day)}
                      title={day.displayDay}
                      temp={day.info.temp}
                      condition={day.info.condition}
                      button={true}
                    />
                  ))}
                </List>
              }
            </Grid>
          </CardContent>
        </Card>
      }
      <Backdrop className={classes.backdrop} open={loading}>
        <CircularProgress color='inherit' />
      </Backdrop>
    </Container>
  );
}

export default App;