import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';

const useStyles = makeStyles({
  listText: {
    width: 'calc(100% / 3)',
    '&:nth-of-type(2)': {
      textAlign: 'center',
    }
  },
  listIcon: {
    width: 'calc(100% / 3)',
    display: 'flex',
    flexDirection: 'row-reverse',
    '& img': {
      width: '40px',
    },
  },

  hourlyListItem: {
    '&:nth-child(even)': {
      backgroundColor: '#FAFAFA',
    }
  }
});

const ForecastListItem = props => {
  const { title, temp, condition, button } = props;

  const classes = useStyles();
  return (
    <ListItem button={button} onClick={props.onClick} component='li' className={classes.hourlyListItem}>
      <ListItemText primary={title} className={classes.listText} />
      <ListItemText primary={`${temp.avgTempC}°C / ${temp.avgTempF}°F`} className={classes.listText} />
      <ListItemIcon className={classes.listIcon}>
        <img src={`http://openweathermap.org/img/wn/${condition.icon}@2x.png`} alt={condition.main} />
      </ListItemIcon>
    </ListItem>
  )
}

export default ForecastListItem;