import React from 'react';
import ForecastListItem from './ForecastListItem';

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';

const ForecastItem = props => {
  const { displayDay, displayDate, temp, wind, condition, hourlyData } = props;

  const useStyles = makeStyles({
    list: {
      paddingTop: 0,
      '& li': {
        textAlign: 'center',
        '& div': {
          margin: 0,
        }
      }
    },
    listItem: {
      padding: 0,
    },
    listItemLabel: {
      paddingTop: 0,
      margin: -5,
    },
    listItem2: {
      textAlign: 'left',
      '&:nth-of-type(2)': {
        textAlign: 'right',
      }
    },
    weatherIcon: {
      width: 100,
    },
  });

  const classes = useStyles();
  return (
    <Grid>
      <List component='ul' className={classes.list}>
        <ListItem component='li' className={classes.listItem}>
          <ListItemText primary={displayDay} secondary={displayDate} />
        </ListItem>
        <ListItemIcon>
          <img className={classes.weatherIcon} src={`http://openweathermap.org/img/wn/${condition.icon}@2x.png`} alt={condition.main} />
        </ListItemIcon>
        <ListItem component='li' className={classes.listItemLabel}>
          <ListItemText primary={condition.main} secondary={condition.description} />
        </ListItem>
        <ListItem component='li' className={classes.listItem2}>
          <ListItemText primary={`${temp.maxTempC}°C / ${temp.maxTempF}°F`} secondary={`${temp.minTempC}°C / ${temp.minTempF}°F`} />
          <ListItemText primary={`${wind.windSpeedKMH}kmh / ${wind.windSpeedMPH}mph`} secondary={wind.direction} />
        </ListItem>
      </List>
      <List component='ul'>
        {hourlyData && hourlyData.map((day, index) => (
          <ForecastListItem
            key={index}
            title={day.displayTime}
            temp={day.main}
            condition={day.weather[0]}
          />
        ))}
      </List>
    </Grid>
  )
}

export default ForecastItem;