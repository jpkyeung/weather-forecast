import moment from 'moment'

export const toCelsius = k => {
  return k - 273.15;
}
export const toFahrenheit = k => {
  return (toCelsius(k) * 9 / 5) + 32;
}

export const toKMH = ms => {
  return ms * 3.6;
}

export const toMPH = ms => {
  return ms * 2.237;
}

const parseDailyData = arr => {
  let totalWSpeed = 0, totalWDeg = 0, totalTemp = 0, avgTemp, minTemp, maxTemp, directions, wAvg, dIndex, wind, wSpeed, temp;
  let condition = getConditions(arr);

  for (let i = 0; i < arr.length; i++) {
    totalWSpeed += arr[i].wind.speed;
    totalWDeg += arr[i].wind.deg;
    totalTemp += arr[i].main.temp;
    if (!minTemp || arr[i].main.temp_min < minTemp) {
      minTemp = arr[i].main.temp_min;
    }
    if (!maxTemp || arr[i].main.temp_max > maxTemp) {
      maxTemp = arr[i].main.temp_max;
    }
  }

  directions = ['N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'ES', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW']
  wAvg = parseFloat((totalWDeg / arr.length).toFixed(1));
  dIndex = Math.round(((wAvg %= 360) < 0 ? wAvg + 360 : wAvg) / 22.5) % 16;
  avgTemp = parseFloat((totalTemp / arr.length).toFixed(1));
  wSpeed = parseFloat((totalWSpeed / arr.length).toFixed(1));

  wind = {
    speed: wSpeed,
    windSpeedKMH: toKMH(wSpeed).toFixed(1),
    windSpeedMPH: toMPH(wSpeed).toFixed(1),
    direction: directions[dIndex],
  }

  temp = {
    avgTemp: avgTemp,
    minTemp: minTemp,
    maxTemp: maxTemp,
    avgTempC: toCelsius(avgTemp).toFixed(1),
    avgTempF: toFahrenheit(avgTemp).toFixed(1),
    minTempC: toCelsius(minTemp).toFixed(1),
    minTempF: toFahrenheit(minTemp).toFixed(1),
    maxTempC: toCelsius(maxTemp).toFixed(1),
    maxTempF: toFahrenheit(maxTemp).toFixed(1),
  }

  return { temp, condition, wind }
}

const getConditions = arr => {
  let counts = arr.reduce((a, c) => {
    a[c.weather[0].id] = {
      ...c.weather[0],
      count: (a[c.weather[0].id] ? a[c.weather[0].id].count : 0) + 1,
    }
    return a;
  }, {});

  let sortedKeys = Object.keys(counts).sort((a, b) => counts[b].count - counts[a].count);

  return counts[sortedKeys[0]];
}

export const parseForecastData = obj => {
  const forecastData = [];

  // current day in location
  const today = moment().utc().add(obj.city.timezone, 'seconds').startOf('day').valueOf() / 1000;
  const day = 24 * 60 * 60;

  for (let i = 0; i <= 5; i++) {
    let start = today - obj.city.timezone + (i * day);
    let end = start + day;
    let date = moment(start * 1000);

    let details = obj.list.filter(item => item.dt >= start && item.dt < end);
    details.forEach(function (item) {
      item.displayTime = moment(moment(item.dt).add(obj.city.timezone, 'seconds').valueOf() * 1000).utc().format('h:mm A');
      item.main.avgTempC = toCelsius(item.main.temp).toFixed(1);
      item.main.avgTempF = toFahrenheit(item.main.temp).toFixed(1);
    })
    forecastData[i] = {
      details: details,
      displayDay: i === 0 ? 'Today' : date.format('dddd'),
      displayDate: date.format('D/M'),
    }
    if (forecastData[i].details.length === 0) {
      delete forecastData[i];
    }
  }

  forecastData.forEach((item) => {
    item.info = parseDailyData(item.details);
  })
  return forecastData;
}