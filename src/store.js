
import { createStore, applyMiddleware } from 'redux';
import forecastReducer from './reducers/forecastReducer';
import thunk from 'redux-thunk';

const store = createStore(forecastReducer, applyMiddleware(thunk));

export default store;