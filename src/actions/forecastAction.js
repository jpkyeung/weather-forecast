import store from '../store';
import config from '../config';

export const fetch_post = () => {
  return {
    type: 'FETCH_FORECAST'
  };
};

export const receive_post = post => {
  return {
    type: 'FETCHED_FORECAST',
    data: post
  };
};

export const receive_error = () => {
  return {
    type: 'RECEIVE_ERROR'
  };
};

export const fetchForecast = location => {
  store.dispatch(fetch_post());
  return dispatch => {
    return fetch(`https://api.openweathermap.org/data/2.5/forecast?q=${location}&APPID=${config.OpenWeatherAPIKey}`)
      .then(data => data.json())
      .then(data => {
        if (data.cod === '404') {
          throw new Error(data.message);
        }
        dispatch(receive_post(data));
      })
      .catch(() => dispatch(receive_error()));
  };
};