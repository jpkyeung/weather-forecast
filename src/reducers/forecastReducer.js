import { parseForecastData } from '../utils/forecastUtils';

const initialState = {
  forecastData: null,
  city: '',
  isFetching: false,
  isError: false
};

const forecastReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'FETCH_FORECAST':
      return {
        isFetching: true,
      }
    case 'FETCHED_FORECAST':
      const forecastData = parseForecastData(action.data);
      return {
        forecastData,
        city: action.data.city,
        isFetching: false,
      }
    case 'RECEIVE_ERROR':
      return {
        isError: true,
        isFetching: false,
      }
    default:
      return state;
  }
};

export default forecastReducer;